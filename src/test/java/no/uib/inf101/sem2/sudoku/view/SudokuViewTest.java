package no.uib.inf101.sem2.sudoku.view;


import no.uib.inf101.sem2.sudoku.model.SudokuModel;
import no.uib.inf101.sem2.sudoku.model.puzzel.LevelState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SudokuViewTest {

    private SudokuView sudokuView;


    private SudokuModel model;

    @BeforeEach
    public void setUp() {
        sudokuView = new SudokuView();
        sudokuView.initGame();
        model = new SudokuModel(LevelState.EASY);


    }


    /**
     * It tests the createNumberPanel() method to ensure that the number panel is
     * created with the correct button text, font size and preferred size.
     */

    @Test
    public void testCreateNumberPanel() {
        JButton button = (JButton) sudokuView.createNumberPanel().getComponent(1);

        assertEquals(2, Integer.parseInt(button.getText()));
        assertEquals(30, button.getFont().getSize());
        assertEquals(new Dimension(60, 60), button.getPreferredSize());
    }

    /**
     * It tests the drawGridPanel() method to ensure that the grid panel is
     * drawn correctly with the appropriate button text, font size and preferred size.
     */
    @Test
    public void testDrawGridPanel() {

        JPanel gridPanel = sudokuView.createGridPanel(1, 1);
        JButton button = new JButton("1");
        String buttonValue = button.getText();
        String buttontext = buttonValue == "0" ? "" : buttonValue;

        assertEquals(buttontext, button.getText());
        assertEquals(13, button.getFont().getSize());
        assertEquals(new Dimension(75, 29), button.getPreferredSize());

    }


}
