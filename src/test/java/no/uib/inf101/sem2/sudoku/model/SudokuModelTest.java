package no.uib.inf101.sem2.sudoku.model;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.sudoku.model.puzzel.LevelState;
import no.uib.inf101.sem2.sudoku.view.CellButton;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class SudokuModelTest {

    SudokuModel model = new SudokuModel(LevelState.EASY);

    /**
     * Tests whether the solvedBoard and unsolvedBoard are generated correctly.
     * Iterates through the unsolvedBoard and checks that all values are the same as the corresponding values
     * in the solvedBoard.
     */
    @Test
    void generateSolvedAndUnsolvedBoardTest() {
        SudokuBoard solvedBoard = model.solvedBoard;
        SudokuBoard unsolvedBoard = model.board;
        for (GridCell<Integer> unsolvedCell : unsolvedBoard.iterator()) {
            if (unsolvedCell.value() != 0)
                assertEquals((int) unsolvedCell.value(), solvedBoard.get(unsolvedCell.pos()));
        }
    }

    /**
     * Tests whether the cellButtons method returns the expected buttons for the current SudokuModel object.
     * Compares the value of each button to the value in the corresponding cell in the unsolvedBoard.
     */
    @Test
    void cellButtonsTest() {
        SudokuBoard unsolvedBoard = model.board;
        Iterable<CellButton> cellButtons = model.cellButtons();

        for (CellButton button : cellButtons) {
            assertEquals((int) button.cell().value(), unsolvedBoard.get(button.cell().pos()));
        }
    }

    /**
     * Tests the checkValue method with a known valid value and a known invalid value.
     * Also tests that all values in the solvedBoard are valid for their corresponding cell.
     */
    @Test
    void checkValueTest() {
        CellPosition pos = new CellPosition(0, 1);
        int selectedNumber = model.solvedBoard.get(pos);
        assertFalse(model.checkValue(selectedNumber + 1, new CellPosition(0, 1)));

        assertTrue(model.checkValue(selectedNumber, pos));


        for (GridCell<Integer> cell : model.solvedBoard.iterator()) {
            assertTrue(model.checkValue(cell.value(), cell.pos()));
        }
    }

    /**
     * Tests the setValue method by setting a value at a specific cell and checking that the value is set correctly.
     */
    @Test
    void setValueTest() {
        CellPosition pos = new CellPosition(0, 0);
        model.setValue(8, pos);
        assertEquals(model.board.get(pos), 8);


        pos = new CellPosition(0, 1);
        model.setValue(1, pos);
        assertNotEquals(model.board.get(pos), 8);


    }

    @Test
    void getBoardWidthTest() {
        assertEquals(9, model.board.cols);
    }

    @Test
    void getBoardHeightTest() {
        assertEquals(9, model.board.rows);

    }

    /**
     * Tests the cellButtons method by checking that the number of buttons returned is equal to the
     * product of the board's width and height.
     */
    @Test
    public void testCellButtons() {

        Iterable<CellButton> cellButtons = model.cellButtons();

        assertNotNull(cellButtons);

        int expectedSize = model.getBoardWidth() * model.getBoardHeight();
        int actualSize = 0;
        for (Iterator<CellButton> it = cellButtons.iterator(); it.hasNext(); it.next()) {
            actualSize++;
        }

        assertEquals(expectedSize, actualSize);
    }

    @Test
    void checkGameSolved() {

        assertFalse(model.checkGameSolved());


    }
}
