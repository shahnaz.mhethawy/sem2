package no.uib.inf101.sem2.sudoku.model.puzzel;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PuzzleGeneratorTest {

    PuzzelFactory factory;

    /**
     * Tests the generateSolvedBoard() and generateUnsolvedBoard() methods of the PuzzelFactory class.
     * The method generates a solved and an unsolved puzzle using the factory with a specified difficulty level.
     * Then the method checks if each non-zero cell in the unsolved puzzle is equal to its counterpart in the solved puzzle.
     */
    @Test
    void generateSolvedAndUnsolvedBoardTest() {
        factory = new Puzzle(LevelState.EASY);
        int[][] solvedPuzzle = factory.generateSolvedPuzzle();
        int[][] unsolvedPuzzle = factory.generateUnsolvedPuzzle();
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (unsolvedPuzzle[i][j] != 0)
                    assertEquals(unsolvedPuzzle[i][j], solvedPuzzle[i][j]);

            }
        }
    }
}
