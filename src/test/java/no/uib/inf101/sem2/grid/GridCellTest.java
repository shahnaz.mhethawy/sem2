package no.uib.inf101.sem2.grid;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GridCellTest {

    /**
     * Tests if a new grid cell is created with the correct position and value.
     */
    @Test
    void sanityTest() {
        String item = "Test";
        CellPosition pos = new CellPosition(4, 2);
        GridCell<String> gridCell = new GridCell<>(pos, item);

        assertEquals(pos, gridCell.pos());
        assertEquals(item, gridCell.value());
    }

}
