package no.uib.inf101.sem2.grid;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GridTest {
    Grid grid = new Grid(4, 5, 0);

    /**
     * It checks that the dimensions are set correctly and that the default value is set for all cells.
     * It also sets a value for a specific cell and checks that it is set correctly.
     */
    @Test
    void gridSanityTest() {
        int defaultValue = 1;
        Grid grid = new Grid(3, 2, defaultValue);

        assertEquals(3, grid.rows());
        assertEquals(2, grid.cols());

        assertEquals(1, grid.get(new CellPosition(0, 0)));
        assertEquals(1, grid.get(new CellPosition(2, 1)));

        grid.set(new CellPosition(1, 1), 3);
        assertEquals(3, grid.get(new CellPosition(1, 1)));
        assertEquals(1, grid.get(new CellPosition(1, 0)));
        assertEquals(1, grid.get(new CellPosition(0, 1)));
        assertEquals(1, grid.get(new CellPosition(2, 1)));
    }

    /**
     * It checks that the number of rows in the grid is returned correctly.
     */
    @Test
    void TestGetRows() {
        assertEquals(4, grid.rows());
        assertNotEquals(3, grid.rows());
        assertNotEquals(5, grid.rows());
    }


    /**
     * It checks that the number of columns in the grid is returned correctly.
     */
    @Test
    void TestGetCols() {
        assertEquals(5, grid.cols());
        assertNotEquals(3, grid.cols());
        assertNotEquals(6, grid.cols());
    }


    /**
     * It checks that a value can be retrieved from a specific cell and that it can be set for that cell.
     */
    @Test
    void getValueOfPosition() {
        assertEquals(0, grid.get(new CellPosition(3, 4)));
        grid.set(new CellPosition(3, 4), 1);
        assertEquals(1, grid.get(new CellPosition(3, 4)));
    }


    /**
     * It checks that the method returns {@code true} for valid cell positions and {@code false} for invalid positions.
     */
    @Test
    void positionIsOnGridTest() {
        assertTrue(grid.positionIsOnGrid(new CellPosition(3, 4)));
        assertFalse(grid.positionIsOnGrid(new CellPosition(4, 5)));
        assertTrue(grid.positionIsOnGrid(new CellPosition(3, 4)));

    }

    /**
     * It checks that all cells in the grid can be iterated over and that the iterator returns the correct values.
     */
    @Test
    void itratorTest() {
        grid.set(new CellPosition(0, 0), 8);
        grid.set(new CellPosition(0, 1), 7);
        grid.set(new CellPosition(0, 2), 9);
        grid.set(new CellPosition(1, 0), 4);
        grid.set(new CellPosition(1, 4), 9);
        List<GridCell<Integer>> cells = new ArrayList<>();
        for (GridCell<Integer> cell : grid.iterator()) {
            cells.add(cell);
        }
        assertEquals(4 * 5, cells.size());
        assertTrue(cells.contains(new GridCell<>(new CellPosition(1, 0), 4)));
        assertTrue(cells.contains(new GridCell<>(new CellPosition(1, 4), 9)));

        assertTrue(cells.contains(new GridCell<>(new CellPosition(0, 0), 8)));
        assertTrue(cells.contains(new GridCell<>(new CellPosition(0, 1), 7)));
        assertTrue(cells.contains(new GridCell<>(new CellPosition(0, 2), 9)));

    }
}