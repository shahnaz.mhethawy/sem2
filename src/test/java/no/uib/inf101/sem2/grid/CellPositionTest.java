package no.uib.inf101.sem2.grid;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CellPositionTest {

    /**
     * Tests the {@link CellPosition#col()} and {@link CellPosition#row()} methods.
     * Verifies if a newly created CellPosition object's col() and row() methods
     * return the expected values.
     */
    @Test
    void sanityTest() {
        CellPosition pos = new CellPosition(3, 2);
        assertEquals(2, pos.col());
        assertEquals(3, pos.row());
    }

}
