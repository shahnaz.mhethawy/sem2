package no.uib.inf101.sem2.sudoku.model;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.Grid;


/**
 * Represents a Sudoku board, which is a type of grid used for playing Sudoku.
 * Inherits from the Grid class, and adds additional functionality specific to Sudoku.
 */

public class SudokuBoard extends Grid {
    int rows;
    int cols;


    /**
     * Creates a new Sudoku board with the given initial values.
     *
     * @param initBoard The initial values for the board as a 2D array of integers.
     */
    public SudokuBoard(int[][] initBoard) {

        super(initBoard.length, initBoard[0].length, 0);
        this.cols = initBoard[0].length;
        this.rows = initBoard.length;


        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {

                set(new CellPosition(i, j), initBoard[i][j]);
            }

        }

    }
}
