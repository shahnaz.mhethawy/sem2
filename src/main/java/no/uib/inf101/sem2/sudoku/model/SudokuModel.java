package no.uib.inf101.sem2.sudoku.model;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.sudoku.model.puzzel.LevelState;
import no.uib.inf101.sem2.sudoku.model.puzzel.Puzzle;
import no.uib.inf101.sem2.sudoku.view.CellButton;
import no.uib.inf101.sem2.sudoku.view.ViewableSudokuModel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;


/**
 * A class represents the Sudoku game model.
 */
public class SudokuModel implements ViewableSudokuModel {
    public SudokuBoard solvedBoard;
    public SudokuBoard board;
    int[][] unsolvedPuzzle;

    /**
     * Constructs a new SudokuModel with the specified level of difficulty.
     *
     * @param levelState the level of difficulty for the game.
     */
    public SudokuModel(LevelState levelState) {

        Puzzle factory = new Puzzle(levelState);
        int[][] solvedBoard = factory.generateSolvedPuzzle();
        unsolvedPuzzle = factory.generateUnsolvedPuzzle();
        this.board = new SudokuBoard(unsolvedPuzzle);
        this.solvedBoard = new SudokuBoard(solvedBoard);

    }

    @Override
    public void resetUnsolvedBoard() {
        this.board = new SudokuBoard(unsolvedPuzzle);
    }

    /**
     * @return true if the Sudoku is completely solved
     */
    @Override
    public boolean checkGameSolved() {
        for (GridCell<Integer> cell : board.iterator()) {
            if (cell.value() == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns an iterable of CellButton objects representing the buttons on the game board.
     *
     * @return an iterable of CellButton objects.
     */
    @Override
    public Iterable<CellButton> cellButtons() {
        ArrayList<CellButton> cellButtons = new ArrayList<>();

        for (GridCell<Integer> cell : board.iterator()) {

            String buttonString = cell.value() == 0 ? "" : Integer.toString(cell.value());

            JButton button = new JButton(buttonString);
            button.setForeground(Color.CYAN);

            cellButtons.add(new CellButton(cell, button, (getBoardHeight() * cell.pos().row()) + cell.pos().col()));
        }
        // sort cellButtons according to its indexes
        cellButtons.sort(Comparator.comparing(CellButton::index));
        return cellButtons;
    }


    /**
     * @return int cols in board
     */
    @Override
    public int getBoardWidth() {
        return board.cols;
    }

    /**
     * @return int rows in board
     */
    @Override
    public int getBoardHeight() {
        return board.rows;
    }


    /**
     * Checks if the given number matches the value in the solution for the cell at the given position.
     *
     * @param selectedNumber the number to check
     * @param pos            the position of the cell to check
     * @return true if the selected number matches the solution for the cell at the given position, false otherwise
     */
    @Override
    public boolean checkValue(int selectedNumber, CellPosition pos) {
        int solvedCell = solvedBoard.get(pos);
        return solvedCell == selectedNumber;

    }

    /**
     * @param selectedNumber this is the input number
     * @param pos            the position of cell where to input
     */
    @Override
    public void setValue(int selectedNumber, CellPosition pos) {
        board.set(pos, selectedNumber);
    }


}
