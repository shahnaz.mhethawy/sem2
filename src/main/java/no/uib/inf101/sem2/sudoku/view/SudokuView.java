package no.uib.inf101.sem2.sudoku.view;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.sudoku.model.SudokuModel;
import no.uib.inf101.sem2.sudoku.model.puzzel.LevelState;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class SudokuView extends JPanel {


    private final Dimension buttonDimension = new Dimension(60, 60);
    private final Font buttonFont = new Font("Arial", Font.PLAIN, 30);

    int selectedNumber;
    ViewableSudokuModel model;


    JPanel mainPanel;
    JPanel gamePanel;

    Timer timer;

    JLabel timerLabel;
    private int elapsedTime;
    private Iterable<CellButton> cellButtons;

    /**
     * Constructor for SudokuView.
     * Creates the game panel and adds the menu panel to it.
     */
    public SudokuView() {
        this.model = new SudokuModel(LevelState.EASY);


        this.mainPanel = new JPanel(new BorderLayout());
        this.mainPanel.setSize(new Dimension(Toolkit.getDefaultToolkit().getScreenSize()));


        JPanel menuPanel = createMenuPanel();
        this.mainPanel.add(menuPanel, BorderLayout.CENTER);


        this.elapsedTime = 0;
        this.timer = new Timer(1000, e -> {
            this.elapsedTime++;
            // Update the display to show the elapsed time
            timerLabel.setText("Time: " + this.elapsedTime + " seconds");
        });

        // Create the timer label and add it to the frame
        timerLabel = new JLabel("Time: 0 seconds");
        timerLabel.setForeground(Color.DARK_GRAY);
        timerLabel.setPreferredSize(new Dimension(40, 50));
        timerLabel.setFont(new Font("Arial", Font.PLAIN, 18));

    }


    /**
     * Initializes the game by setting the Sudoku model and drawing the grid and number panels.
     */
    protected void initGame() {

        this.mainPanel.removeAll();

        this.cellButtons = model.cellButtons();
        this.gamePanel = createGamePanel();

        this.mainPanel.add(gamePanel, BorderLayout.EAST);

        elapsedTime = 0;
        timer.start();

    }

    /**
     * Creates the number panel for the game that responses for the input.  .
     *
     * @return the number panel
     */

    private JPanel createGamePanel() {
        JPanel gamePanel = new JPanel(new GridBagLayout());

        JPanel gridPanel = createGridPanel(model.getBoardHeight(), model.getBoardWidth());
        JPanel numberPanel = createNumberPanel();
        JButton restartButton = createRestartButton();
        JButton newGameButton = createNewGameButton();


        GridBagConstraints gbc = new GridBagConstraints();


        // add timerLabel to the top left corner
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        gbc.gridheight = 2;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 1;

        gamePanel.add(timerLabel, gbc);

        // add restartButton to the row below timerLabel
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.insets = new Insets(300, 0, 0, 0); // add  padding
        gbc.fill = GridBagConstraints.BOTH;
        gamePanel.add(restartButton, gbc);

        // add newGameButton to the row below timerLabel
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.gridwidth = 1;
        gbc.gridheight = 1;
        gbc.insets = new Insets(20, 0, 250, 0); // add  padding
        gbc.fill = GridBagConstraints.BOTH;
        gamePanel.add(newGameButton, gbc);

        // add gridPanel to the right of timerLabel and restartButton
        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.gridwidth = 7;
        gbc.gridheight = 5;
        gbc.insets = new Insets(0, 10, 0, 0); // add  padding
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 4;
        gamePanel.add(gridPanel, gbc);


        // add numberPanel to the last row of the grid
        gbc.gridx = 2;
        gbc.gridy = 8;
        gbc.gridwidth = 1;
        gbc.gridheight = 2;

        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 1;
        gamePanel.add(numberPanel, gbc);


        gamePanel.setPreferredSize(new Dimension(Toolkit.getDefaultToolkit().getScreenSize()));


        // gamePanel.setBackground(Color.GRAY);

        return gamePanel;
    }

    /**
     * Creates and returns a JPanel with a grid of JButtons, one for each cell in the game board.
     *
     * @param height the number of rows in the game board
     * @param width  the number of columns in the game board
     * @return a JPanel containing a grid of JButtons, one for each cell in the game board
     */
    protected JPanel createGridPanel(int height, int width) {

        JPanel buttonsPanel = new JPanel(new GridLayout(height, width));


        for (CellButton cellButton : this.cellButtons) {

            JButton button = cellButton.button();
            if (cellButton.cell().value() == 0) {
                button.setBackground(Color.GRAY);
            }

            button.setFont(buttonFont);
            button.setPreferredSize(buttonDimension);


            Integer buttonValue = cellButton.cell().value();
            CellPosition buttonPosition = cellButton.cell().pos();


            // Create an action listener for the button
            ActionListener activeButtonListener = e -> {
                if (selectedNumber != 0) {

                    if (model.checkValue(selectedNumber, buttonPosition)) {
                        model.setValue(selectedNumber, buttonPosition);
                        button.setForeground(Color.BLACK);

                        //Deactivate button by remove all actionListener
                        for (ActionListener listener : button.getActionListeners()) {
                            button.removeActionListener(listener);
                        }

                        if (model.checkGameSolved()) {
                            timer.stop();
                            gamePanel.setVisible(false);
                            this.mainPanel.add(createGameStateScreen(), BorderLayout.CENTER);
                        }

                    } else {
                        // If the input not the right one for the cell, change the button color to red
                        button.setForeground(Color.RED);
                    }

                    // Set the text of the button to the selected number
                    button.setText(Integer.toString(selectedNumber));
                    selectedNumber = 0;
                }


            };

            // If the cell has a value of 0, add the action listener to the button
            if (buttonValue == 0) {
                button.addActionListener(activeButtonListener);
            }

            button.setBackground(Color.red);
            // Add the button to the grid panel at the appropriate index based on its position in the game board
            buttonsPanel.add(button, cellButton.index());
        }
        return buttonsPanel;
    }

    /**
     * Creates the number panel for the game that responses for the input.  .
     *
     * @return the number panel
     */

    protected JPanel createNumberPanel() {
        JPanel numberPanel = new JPanel();
        // numberPanel.setPreferredSize(new Dimension(200, 75));

        numberPanel.setFont(buttonFont);
        //numberPanel.setBackground(Color.LIGHT_GRAY);


        numberPanel.setLayout(new GridLayout(1, 9, 1, 0));

        numberPanel.setBorder(BorderFactory.createEmptyBorder(15, 0, 3, 0));


        for (int i = 1; i <= 9; i++) {
            JButton button = new JButton(Integer.toString(i));
            button.setFont(buttonFont);
            button.setPreferredSize(buttonDimension);

            button.addActionListener(e -> selectedNumber = Integer.parseInt(button.getText()));
            numberPanel.add(button);
        }


        return numberPanel;


    }

    private JPanel createGameStateScreen() {
        JPanel statePanel = new JPanel(new GridLayout(2, 1));

        JLabel label1 = new JLabel("GOOD JOB!");
        JLabel label2 = new JLabel(" You solved it with in: " + elapsedTime + " sec");

        label1.setFont(new Font("Arial", Font.BOLD, 75));
        label2.setFont(new Font("Arial", Font.ITALIC, 75));

        label1.setBorder(BorderFactory.createEmptyBorder(100, 400, 0, 0));
        label2.setBorder(BorderFactory.createEmptyBorder(10, 200, 300, 0));
        statePanel.add(label1);
        statePanel.add(label2);


        return statePanel;

    }

    private JButton createRestartButton() {
        JButton restartButton = new JButton("Restart ");
        restartButton.setFont(new Font("Arial", Font.ITALIC, 18));
        restartButton.setForeground(Color.BLACK);
        restartButton.setBackground(Color.GREEN);
        restartButton.setPreferredSize(new Dimension(40, 50));
//        restartButton.setBorder(BorderFactory.createEmptyBorder(0, 0, 300, 0));
        restartButton.addActionListener(e -> {

            restartGame();
        });
        return restartButton;
    }

    private JButton createNewGameButton() {
        JButton newGameButton = new JButton("New Game ");
        newGameButton.setFont(new Font("Arial", Font.ITALIC, 18));
        newGameButton.setForeground(Color.BLACK);


        newGameButton.setPreferredSize(new Dimension(40, 50));
//        newGameButton.setBorder(BorderFactory.createEmptyBorder(0, 0, 300, 0));
        newGameButton.addActionListener(e -> {
            gamePanel.setVisible(false);
            mainPanel.add(createMenuPanel());

        });
        newGameButton.setBackground(Color.red);
        return newGameButton;
    }

    private void restartGame() {
        mainPanel.setVisible(false);
        model.resetUnsolvedBoard();
        this.cellButtons = model.cellButtons();
        this.mainPanel.removeAll();

        this.mainPanel.add(new JLabel("Restart...."));
        mainPanel.setVisible(true);
        timer.stop();

        initGame();

    }

    /**
     * Creates the menu panel for the game.
     *
     * @return the menu panel
     */
    protected JPanel createMenuPanel() {
        JPanel menuPanel = new JPanel(new GridLayout(3, 1, 0, 9));
        menuPanel.setFont(buttonFont);
        menuPanel.setBorder(BorderFactory.createEmptyBorder(100, 500, 500, 500));
        menuPanel.setBackground(Color.CYAN);

        for (LevelState l : LevelState.values()) {
            JButton button = new JButton(l.toString());
            button.setFont(buttonFont);


            button.addActionListener(e -> {
                this.model = new SudokuModel(l);
                menuPanel.setVisible(false);
                initGame();


            });
            menuPanel.add(button);

        }
        return menuPanel;
    }


    /**
     * Getter for the game panel.
     *
     * @return the game panel
     */
    public JPanel getMainPanel() {
        return this.mainPanel;
    }
}



