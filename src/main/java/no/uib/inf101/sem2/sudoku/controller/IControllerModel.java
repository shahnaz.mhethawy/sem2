package no.uib.inf101.sem2.sudoku.controller;


/**
 * The IControllerModel interface represents the controller for a Sudoku game,
 * responsible for managing the interaction between the user and the game model.
 * It defines a  method, {@code startGame()}, which starts the Sudoku game
 * by setting up the game frame and showing the game panel.
 */

public interface IControllerModel {

    /**
     * Starts the Sudoku game by setting up the game frame and showing the game panel.
     */
    void startGame();

}
