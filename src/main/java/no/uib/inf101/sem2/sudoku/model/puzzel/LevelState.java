package no.uib.inf101.sem2.sudoku.model.puzzel;


/**
 * An enum representing the different difficulty levels of a Sudoku puzzle.
 * <p>
 * Each level has a corresponding complexity value, indicating the number of
 * cells to be left empty in an unsolved puzzle.
 * <p>
 */
public enum LevelState {
    EASY(1),
    MEDIUM(5),
    HARD(6);
    final int complicity;

    LevelState(int complicity) {
        this.complicity = complicity;
    }
}
