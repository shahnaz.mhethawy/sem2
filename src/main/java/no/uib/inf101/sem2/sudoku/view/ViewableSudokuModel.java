package no.uib.inf101.sem2.sudoku.view;

import no.uib.inf101.sem2.grid.CellPosition;

public interface ViewableSudokuModel {


    /**
     * Returns an iterable of CellButton objects representing the buttons on the game board.
     *
     * @return an iterable of CellButton objects.
     */
    Iterable<CellButton> cellButtons();

    /**
     * @return int cols in board
     */
    int getBoardWidth();

    /**
     * @return int rows in board
     */
    int getBoardHeight();


    /**
     * Checks if the given number matches the value in the solution for the cell at the given position.
     *
     * @param selectedNumber the number to check
     * @param pos            the position of the cell to check
     * @return true if the selected number matches the solution for the cell at the given position, false otherwise
     */
    boolean checkValue(int selectedNumber, CellPosition pos);

    /**
     * set select number in the specific position.
     *
     * @param selectedNumber that is the input
     * @param pos            the position of cell that the selectNumber will poste in,
     */
    void setValue(int selectedNumber, CellPosition pos);


    void resetUnsolvedBoard();

    /**
     * @return true if the Sudoku is completely solved
     */
    boolean checkGameSolved();

}
