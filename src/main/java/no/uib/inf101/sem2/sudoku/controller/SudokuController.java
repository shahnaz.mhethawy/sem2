package no.uib.inf101.sem2.sudoku.controller;

import no.uib.inf101.sem2.sudoku.view.SudokuView;

import javax.swing.*;
import java.awt.*;


/**
 * The SudokuController class is responsible for controlling the flow of the Sudoku game.
 * It creates a new SudokuView object to display the game, and manages the interaction
 * between the user and the game.
 */

public class SudokuController implements IControllerModel {


    private final SudokuView view;

    private final JFrame gameFrame;

    /**
     * Creates a new SudokuController object.
     */
    public SudokuController() {
        this.view = new SudokuView();
        this.gameFrame = new JFrame();
    }

    /**
     * Starts the Sudoku game by setting up the game frame and showing the game panel.
     */
    @Override
    public void startGame() {
        gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gameFrame.setTitle("Sudoku");
        gameFrame.setSize(Toolkit.getDefaultToolkit().getScreenSize());

        gameFrame.setVisible(true);
        gameFrame.setContentPane(view.getMainPanel());
    }
}
