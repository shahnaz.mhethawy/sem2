package no.uib.inf101.sem2.sudoku.model.puzzel;

import java.util.Random;

public class Puzzle implements PuzzelFactory {
    Random rand = new Random();
    int complicity;
    int[][] board;

    public Puzzle(LevelState levelState) {
        this.complicity = levelState.complicity;
        this.board = new int[][]{
                {1, 2, 3, 4, 5, 6, 7, 8, 9},
                {4, 5, 6, 7, 8, 9, 1, 2, 3},
                {7, 8, 9, 1, 2, 3, 4, 5, 6},

                {2, 3, 1, 5, 6, 4, 8, 9, 7},
                {5, 6, 4, 8, 9, 7, 2, 3, 1},
                {8, 9, 7, 2, 3, 1, 5, 6, 4},

                {3, 1, 2, 6, 4, 5, 9, 7, 8},
                {6, 4, 5, 9, 7, 8, 3, 1, 2},
                {9, 7, 8, 3, 1, 2, 6, 4, 5}
        };
        shuffleBoard();

    }


    private void swapCols(int colStart) {

        for (int i = 0; i < 9; i++) {
            int temp = board[i][colStart];
            board[i][colStart] = board[i][colStart + 2];

            board[i][colStart + 2] = temp;
        }
    }

    private void swapRows(int rowStart) {

        for (int col = 0; col < 9; col++) {
            int temp = board[rowStart][col];
            board[rowStart][col] = board[rowStart + 2][col];
            board[rowStart + 2][col] = temp;
        }
    }

    private void shuffleBoard() {

        int[] numbers = {0, 3, 6};
        int num = rand.nextInt(3);
        int start = numbers[num];
        swapCols(start);
        swapRows(start);


    }

    /**
     * Generates an unsolved Sudoku puzzle board.
     *
     * @return a 2D integer array representing the unsolved board
     */
    public int[][] generateUnsolvedPuzzle() {
        int[][] unsolvedBoard = new int[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (rand.nextInt(9) < complicity) {
                    unsolvedBoard[i][j] = 0;
                } else {
                    unsolvedBoard[i][j] = board[i][j];
                }

            }
        }

        return unsolvedBoard;

    }

    public int[][] createDeepCopy(Puzzle original) {
        int[][] originalBoard = generateUnsolvedPuzzle();
        int[][] newBoard = new int[originalBoard.length][originalBoard[0].length];
        for (int i = 0; i < originalBoard.length; i++) {
            for (int j = 0; j < originalBoard[i].length; j++) {
                newBoard[i][j] = originalBoard[i][j];
            }
        }
        return newBoard;
    }

    /**
     * Generates a solved Sudoku puzzle board.
     *
     * @return a 2D integer array representing the solved board
     */
    public int[][] generateSolvedPuzzle() {
        int[][] solvedBoard = new int[9][9];
        for (int i = 0; i < 9; i++) {
            System.arraycopy(board[i], 0, solvedBoard[i], 0, 9);
        }

        return solvedBoard;

    }

}
