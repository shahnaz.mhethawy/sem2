package no.uib.inf101.sem2.sudoku.view;

import no.uib.inf101.sem2.grid.GridCell;

import javax.swing.*;

/**
 * A record is used to create an immutable object.
 * Represents a button associated with a GridCell<Integer> object, along with its index.
 *
 * @param cell   the GridCell<Integer> object associated with the button
 * @param button the JButton object associated with the cell
 * @param index  the index of the cell in the grid
 */

public record CellButton(GridCell<Integer> cell, JButton button, Integer index) {


}

