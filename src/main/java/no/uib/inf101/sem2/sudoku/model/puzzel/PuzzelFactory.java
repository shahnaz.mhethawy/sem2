package no.uib.inf101.sem2.sudoku.model.puzzel;


/**
 * An interface for generating Sudoku puzzle boards.
 */
public interface PuzzelFactory {

    /**
     * Generates a solved Sudoku puzzle board.
     *
     * @return a 2D integer array representing the solved board
     */
    int[][] generateSolvedPuzzle();


    /**
     * Generates an unsolved Sudoku puzzle board.
     *
     * @return a 2D integer array representing the unsolved board
     */
    int[][] generateUnsolvedPuzzle();

}
