package no.uib.inf101.sem2;

import no.uib.inf101.sem2.sudoku.controller.IControllerModel;
import no.uib.inf101.sem2.sudoku.controller.SudokuController;

public class Main {

    /**
     * The main method is the entry point for the Sudoku game.
     * It creates a new SudokuController object and starts the game by calling its {@code startGame()} method.
     */
    public static void main(String[] args) {

        IControllerModel controller = new SudokuController();
        controller.startGame();

    }
}
