package no.uib.inf101.sem2.grid;


/**
 * A record representing a position of a cell in a grid.
 */
public record CellPosition(int row, int col) {

}

