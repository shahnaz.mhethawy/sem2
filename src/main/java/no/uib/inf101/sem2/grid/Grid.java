package no.uib.inf101.sem2.grid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Represents a two-dimensional grid of integers, implemented as a hash map where each key is a CellPosition object
 * and each value is an integer.
 */
public class Grid implements IGrid {
    HashMap<CellPosition, Integer> cells;
    private int rows;

    private int cols;

    /**
     * Constructs a new grid with the specified number of rows and columns,
     * and fills it with the specified initial value.
     *
     * @param rows    the number of rows in the grid
     * @param cols    the number of columns in the grid
     * @param initial the initial value to fill the grid with
     */
    public Grid(int rows, int cols, int initial) {
        if (!(rows <= 0 || cols <= 0)) {
            this.rows = rows;
            this.cols = cols;
            this.cells = new HashMap<>();
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    cells.put(new CellPosition(i, j), initial);
                }
            }
        }
    }

    /**
     * Sets the value of the cell at the specified position.
     *
     * @param pos   the position of the cell to set
     * @param value the value to set the cell to
     */
    // set the value at the particular position in the grid
    @Override
    public void set(CellPosition pos, int value) {
        if (this.positionIsOnGrid(pos)) {
            cells.put(pos, value);
        }
    }

    // get the value of the particular position.
    @Override
    public int get(CellPosition pos) {
        return cells.get(pos);
    }

    /**
     * Checks if the specified position is on the grid.
     *
     * @param pos the position to check
     * @return true if the position is on the grid, false otherwise
     */
    @Override
    public boolean positionIsOnGrid(CellPosition pos) {
        return cells.containsKey(pos);
    }

    /**
     * An iterable of GridCell objects that represent the cells in the grid.
     *
     * @return an iterable of GridCell objects
     */
    @Override
    public Iterable<GridCell<Integer>> iterator() {
        List<GridCell<Integer>> gridCells = new ArrayList<>(cells.size());
        for (CellPosition pos : cells.keySet()) {
            GridCell<Integer> cell = new GridCell<>(pos, cells.get(pos));
            gridCells.add(cell);

        }
        return gridCells;
    }

    /**
     * Gets the number of rows in the grid.
     *
     * @return the number of rows in the grid
     */

    @Override
    public int rows() {
        return this.rows;
    }

    /**
     * Gets the number of columns in the grid.
     *
     * @return the number of columns in the grid
     */
    @Override
    public int cols() {
        return this.cols;
    }

}
