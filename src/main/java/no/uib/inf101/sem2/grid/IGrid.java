package no.uib.inf101.sem2.grid;

public interface IGrid extends GridDimension {

    /**
     * Sets the value of the cell at the specified position.
     *
     * @param pos   the position of the cell to set
     * @param value the value to set the cell to
     */
    void set(CellPosition pos, int value);


    /**
     * Gets the value of the cell at the specified position.
     *
     * @param pos the position of the cell to get
     * @return the value of the cell at the specified position
     */
    int get(CellPosition pos);


    /**
     * An iterable of GridCell objects that represent the cells in the grid.
     *
     * @return an iterable of GridCell objects
     */
    Iterable<GridCell<Integer>> iterator();


    /**
     * Reports whether the position is within bounds for this grid
     *
     * @param pos position to check
     * @return true if the coordinate is within bounds, false otherwise
     */

    boolean positionIsOnGrid(CellPosition pos);


}
