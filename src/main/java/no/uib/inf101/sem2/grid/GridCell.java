package no.uib.inf101.sem2.grid;

/**
 * A record representing a cell in a grid.
 *
 * @param <E> the type of value stored in the cell
 */
public record GridCell<E>(CellPosition pos, E value) {

}




