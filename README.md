
# Sudoku Game

## Introduction

This project is a Java-based Sudoku game built using Java Swing. The game allows users to play and solve Sudoku puzzles on their computer.

## Features

1. Supports standard 9x9 Sudoku puzzles.
2. Uses Java Swing for a user-friendly interface.
3. Highlights invalid entries in real-time.
4. Provides a control panel for easy interaction with the game.

## Requirements

To run the game, you will need to have the following installed on your computer:

- Java Development Kit (JDK) version 8 or later
- Apache Maven 3.6.3 or later

## Installation

To install and run the game, follow these steps:

1. Clone this repository to your local machine
2. Open a terminal or command prompt and navigate to the project directory
3. Run the following command to build the game:

   ```
   mvn clean package
   ```
   
4. Run the following command to start the game:

   ```
   java -jar target/sudoku-game.jar
   ```

## Usage

Once the game is running, follow these steps to play Sudoku:

1. Choose a difficulty level for the puzzle.
2. Click on number from the number keys and then chose which cell to fill.
3. If the numbers are valid, the cell will turn green. Otherwise, it will turn red.
4. Continue filling in the cells until the puzzle is complete.

.

## Credits

Java Swing: https://docs.oracle.com/javase/tutorial/uiswing/
Java Standard Library: https://docs.oracle.com/javase/8/docs/api/
